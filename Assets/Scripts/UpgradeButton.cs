﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpgradeButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private UpgradeType upgradeType;
    public UpgradeType UpgradeType => upgradeType;

    private int id;

    private Game game;

    private PlayerInfo playerInfo;

    private void Start()
    {
        id = GetComponentInParent<Upgrade>().Id;
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        playerInfo = game.State.PlayerInfos.First(info => info.Id == id);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (CanUpgrade())
        {
            game.Upgrade(id, upgradeType);
        }
    }

    private bool CanUpgrade()
    {
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                return playerInfo.DamageUpgrade < PlayerInfo.MAX_UPGRADES &&
                    game.State.Money >= playerInfo.GetCostForNext(playerInfo.DamageUpgrade);
            case UpgradeType.RateOfFire:
                return playerInfo.RateOfFireUpgrade < PlayerInfo.MAX_UPGRADES &&
                    game.State.Money >= playerInfo.GetCostForNext(playerInfo.RateOfFireUpgrade);
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
