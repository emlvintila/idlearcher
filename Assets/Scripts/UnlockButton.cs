﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UnlockButton : MonoBehaviour
{
    public Color AvailableColor = new Color(0.2f, 0.4117647f, 0.1176471f, 1f);

    private Button button;

    private IEnumerable<CanvasRenderer> canvasRenderers;

    private Game game;

    private int id;
    public GameObject Player;

    private PlayerInfo playerInfo;

    private Text text;

    private void Start()
    {
        if (!Player)
            Player = Resources.FindObjectsOfTypeAll<Player>().First(p => p.Id == id).gameObject;

        id = GetComponentInParent<Upgrade>().Id;
        text = GetComponentInChildren<Text>();
        button = GetComponent<Button>();
        button.onClick.AddListener(OnButtonClick);
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        playerInfo = game.State.PlayerInfos.First(info => info.Id == id);
        canvasRenderers = GetComponentsInChildren<CanvasRenderer>();
        if (game.State.UnlockedPlayers.Contains(id))
        {
            DisableOverlay();
            DisableButton();
            EnablePlayer();
        }

        decimal cost = playerInfo.UnlockCost;
        #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
        string t = cost.ToString(CultureInfo.InvariantCulture);
        #else
            string t = Helpers.CurrencyString(cost);
        #endif
        text.SetText(t);
    }

    private void OnGUI()
    {
        if (CanUnlock())
        {
            if (text.color != AvailableColor)
                text.color = AvailableColor;
        }
        else
        {
            if (text.color != Color.black)
                text.color = Color.black;
        }
    }

    private bool CanUnlock() => game.State.Money >= playerInfo.UnlockCost;

    public void OnButtonClick()
    {
        if (CanUnlock())
        {
            game.Unlock(id);
            DisableButton();
            DisableOverlay();
            EnablePlayer();
        }
    }

    private void DisableOverlay()
    {
        gameObject.transform.parent.gameObject.GetComponent<Image>().raycastTarget = false;
    }

    private void DisableButton()
    {
        foreach (CanvasRenderer canvasRenderer in canvasRenderers)
            canvasRenderer.cull = true;
    }

    private void EnablePlayer()
    {
        Player.SetActive(true);
    }
}
