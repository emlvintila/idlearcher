﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CenterOfMassOffset : MonoBehaviour
{
    public Vector2 Delta;

    private void Start()
    {
        GetComponent<Rigidbody2D>().centerOfMass += Delta;
    }
}