﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

public class Game : MonoBehaviour
{
    // ReSharper disable once InconsistentNaming
    public UpgradeEvent OnUpgrade;

    public GameState State { get; private set; }

    public ColorFader ColorFader { get; private set; }

    private void Start()
    {
        ColorFader = GetComponent<ColorFader>();
        LoadState();
        Player[] players = Resources.FindObjectsOfTypeAll<Player>();
        Unlock(1);
        foreach (int id in State.UnlockedPlayers)
            players.First(player => player.Id == id).gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        SaveState();
    }

    public void SaveState()
    {
        Helpers.SerializeToFile(State, Path.Combine(Application.streamingAssetsPath, "state.data"));
    }

    private void LoadState()
    {
        try
        {
            State = Helpers.DeserializeFromFile<GameState>(Path.Combine(Application.streamingAssetsPath,
                "state.data"));
        }
        catch (Exception
            #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
            ex
            #endif
        )
        {
            #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
            Debug.unityLogger.LogException(ex);
            #endif
            State = new GameState();
        }
    }

    public void Unlock(int id)
    {
        PlayerInfo playerInfo = State.PlayerInfos.First(info => info.Id == id);
        // ReSharper disable once InvertIf
        if (!State.UnlockedPlayers.Contains(id) && State.Money >= playerInfo.UnlockCost)
        {
            State.UnlockedPlayers.Add(id);
            State.Money -= playerInfo.UnlockCost;
        }
    }

    public void Upgrade(int id, UpgradeType upgrade)
    {
        PlayerInfo playerInfo = State.PlayerInfos.First(info => info.Id == id);
        switch (upgrade)
        {
            case UpgradeType.Damage:
            {
                decimal cost = playerInfo.GetCostForNext(playerInfo.DamageUpgrade);
                if (playerInfo.DamageUpgrade < PlayerInfo.MAX_UPGRADES && State.Money >= cost)
                {
                    playerInfo.DamageUpgrade++;
                    State.Money -= cost;
                }
                break;
            }

            case UpgradeType.RateOfFire:
            {
                decimal cost = playerInfo.GetCostForNext(playerInfo.RateOfFireUpgrade);
                if (playerInfo.RateOfFireUpgrade < PlayerInfo.MAX_UPGRADES && State.Money >= cost)
                {
                    playerInfo.RateOfFireUpgrade++;
                    State.Money -= cost;
                }

                break;
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(upgrade), upgrade, null);
        }
        OnUpgrade.Invoke(id, upgrade);
    }

    public void UpgradeDamage(int id)
    {
        Upgrade(id, UpgradeType.Damage);
    }

    public void UpgradeRateOfFire(int id)
    {
        Upgrade(id, UpgradeType.RateOfFire);
    }
}
