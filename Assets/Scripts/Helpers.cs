﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public static class Helpers
{
    public static void SerializeToFile(object obj, string path)
    {
        string[] parts = path.Split(Path.DirectorySeparatorChar);
        DirectoryInfo dir = new DirectoryInfo(string.Join(Path.DirectorySeparatorChar.ToString(), parts.Take(parts.Length - 1)));
        if (!dir.Exists)
        {
            dir.Create();
        }

        using (FileStream f = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(f, obj);
        }
    }

    public static T DeserializeFromFile<T>(string path)
    {
        using (FileStream f = new FileStream(path, FileMode.Open))
        {
            BinaryFormatter bf = new BinaryFormatter();
            return (T) bf.Deserialize(f);
        }
    }

    public static string CurrencyString(decimal val)
    {
        if (val > 1000000000000)
        {
            return (val / 1000000000000).ToString("F1") + "T";
        }

        if (val > 1000000000)
        {
            return (val / 1000000000).ToString("F1") + "B";
        }

        if (val > 1000000)
        {
            return (val / 1000000).ToString("F1") + "M";
        }

        if (val > 1000)
        {
            return (val / 1000).ToString("F1") + "K";
        }

        return val.ToString("F0");
    }

    public static void Transparent(this Color color)
    {
        SetAlpha(ref color, 0);
    }

    public static void Opaque(this Color color)
    {
        SetAlpha(ref color, 1);
    }

    public static void Alpha(this Color color, float alpha)
    {
        SetAlpha(ref color, alpha);
    }

    private static void SetAlpha(ref Color color, float alpha)
    {
        color.a = alpha;
    }

    public static void SetText(this Text text, string value)
    {
        text.text = value;
    }
}