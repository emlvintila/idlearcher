﻿using UnityEngine.Events;

public class UpgradeEvent : UnityEvent<int, UpgradeType>
{
}