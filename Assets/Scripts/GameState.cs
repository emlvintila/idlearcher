﻿using System;
using System.Collections.Generic;

[Serializable]
public class GameState
{
    public decimal Money;

    public readonly List<int> UnlockedPlayers = new List<int>();

    public int SelectedPlayer = 0;

    public readonly List<PlayerInfo> PlayerInfos = PlayerInfoFactory.CreateAll();
}