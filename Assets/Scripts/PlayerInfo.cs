﻿using System;

[Serializable]
public class PlayerInfo
{
    public PlayerInfo(int id, decimal unlockCost, int? level = null)
    {
        Id = id;
        UnlockCost = unlockCost;
        Level = level ?? id;
    }

    public int Id { get; }

    public decimal UnlockCost { get; }

    public int Level { get; set; }

#region Damage

    private int damageUpgrade;
    public int DamageUpgrade
    {
        get { return damageUpgrade; }
        set
        {
            if (value > MAX_UPGRADES)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            damageUpgrade = value;
        }
    }

    public float DamageMultiplier { get; set; }

    public float Damage => DamageUpgrade * DamageMultiplier;

#endregion

#region RateOfFire

    private int rateOfFireUpgrade;
    public int RateOfFireUpgrade
    {
        get { return rateOfFireUpgrade; }
        set
        {
            if (value > MAX_UPGRADES)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            rateOfFireUpgrade = value;
        }
    }

    public float RateOfFireMultiplier { get; set; }

    public float RateOfFire => RateOfFireUpgrade * RateOfFireMultiplier;

#endregion

    public decimal UpgradeCostMultiplier { get; set; }

    public const int MAX_UPGRADES = 10;

    public decimal GetCostForNext(int current)
    {
        //long cost = (long) Mathf.Ceil(
        //    Mathf.Pow(Id, 2) * current * Mathf.CeilToInt((DamageMultiplier + RateOfFireMultiplier) / 2)
        //);
        decimal cost = current * UpgradeCostMultiplier;
        return cost;
    }
}