﻿using UnityEngine;

public class ActiveStateToggler : MonoBehaviour
{
    public void Toggle()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}