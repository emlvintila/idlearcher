﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UpgradeProgressBar : MonoBehaviour
{
    // ReSharper disable once InconsistentNaming
    public UpgradeType UpgradeType;

    private Game game;

    private Upgrade upgrade;

    private PlayerInfo playerInfo;

    private Image image;

    private void Start()
    {
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        upgrade = GetComponentInParent<Upgrade>();
        playerInfo = game.State.PlayerInfos.First(p => p.Id == upgrade.Id);
        image = GetComponent<Image>();
    }

    private void Update()
    {
        float progress = GetProgress();
        if (Math.Abs(image.fillAmount - progress) > 1e-3)
        {
            image.fillAmount = progress;
        }
    }

    private float GetProgress()
    {
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                return (float) playerInfo.DamageUpgrade / PlayerInfo.MAX_UPGRADES;
            case UpgradeType.RateOfFire:
                return (float) playerInfo.RateOfFireUpgrade / PlayerInfo.MAX_UPGRADES;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
