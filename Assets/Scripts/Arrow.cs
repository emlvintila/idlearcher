﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class Arrow : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;
    public float RotationSpeed
    {
        get { return rotationSpeed; }
        set { rotationSpeed = value; }
    }

    [SerializeField]
    private float damage;
    public float Damage => damage;

    private new Rigidbody2D rigidbody2D;

    private SpriteRenderer spriteRenderer;

    private Game game;

    private Coroutine fadeCoroutine;

    private bool init = false;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        game = GameObject.FindWithTag("Game").GetComponent<Game>();

        init = true;
    }

    private void FixedUpdate()
    {
        transform.right = Vector3.Slerp(transform.right, rigidbody2D.velocity, RotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D colliderParam)
    {
        if (colliderParam.gameObject.CompareTag("Target"))
        {
            game.State.Money += (decimal)Damage;
        }
        if (colliderParam.gameObject.CompareTag("Target") || colliderParam.gameObject.CompareTag("Terrain"))
        {
            fadeCoroutine = game.ColorFader.Fade(spriteRenderer, true);
            rigidbody2D.bodyType = RigidbodyType2D.Static;
        }
    }

    private void OnEnable()
    {
        if (!init)
        {
            return;
        }

        if (fadeCoroutine != null)
        {
            game.ColorFader.StopCoroutine(fadeCoroutine);
        }

        Color c = spriteRenderer.color;
        c.a = 1;
        spriteRenderer.color = c;
        rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
    }

    private void OnDisable()
    {
        //rigidbody2D.bodyType = RigidbodyType2D.Static;
    }

    public void SetDamage(float newDamage)
    {
        this.damage = newDamage;
    }
}