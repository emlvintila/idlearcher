﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpgradeLevelText : MonoBehaviour
{
    private Upgrade upgrade;

    private Game game;

    private PlayerInfo playerInfo;

    private Text text;

    private int currentLevel;

    private void Start()
    {
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        upgrade = GetComponentInParent<Upgrade>();
        playerInfo = game.State.PlayerInfos.First(p => p.Id == upgrade.Id);
        text = GetComponent<Text>();
        currentLevel = playerInfo.Level;

        game.OnUpgrade.AddListener(OnUpgradeCallback);
        SetText();
    }

    private void OnUpgradeCallback(int id, UpgradeType upgradeType)
    {
        // ReSharper disable once InvertIf
        if (id == upgrade.Id && playerInfo.Level != currentLevel)
        {
            currentLevel = playerInfo.Level;
            SetText();
        }
    }

    private void SetText()
    {
        text.SetText("Level " + currentLevel);
    }
}
