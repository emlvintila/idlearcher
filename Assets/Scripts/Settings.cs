﻿using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public GameObject MusicButtonOn, MusicButtonOff, SfxButtonOn, SfxButtonOff;

    public Slider VolumeSlider;

    private const string VOLUME_KEY = "Volume";

    private const string MUSIC_KEY = "Music";

    private const string SFX_KEY = "SFX";

    private void Awake()
    {
        float volume = PlayerPrefs.GetFloat(VOLUME_KEY, 0.5f);
        VolumeSlider.value = volume;

        bool music = PlayerPrefs.GetInt(MUSIC_KEY, 1) != 0;
        MusicButtonOn.SetActive(music);
        MusicButtonOff.SetActive(!music);

        bool sfx = PlayerPrefs.GetInt(SFX_KEY, 1) != 0;
        SfxButtonOn.SetActive(sfx);
        SfxButtonOff.SetActive(!sfx);
    }

    public void Save()
    {
        float volume = VolumeSlider.value;
        int music = MusicButtonOn.activeSelf ? 1 : 0;
        int sfx = MusicButtonOff.activeSelf ? 1 : 0;
        PlayerPrefs.SetFloat(VOLUME_KEY, volume);
        PlayerPrefs.SetInt(MUSIC_KEY, music);
        PlayerPrefs.SetInt(SFX_KEY, sfx);
        PlayerPrefs.Save();
    }
}