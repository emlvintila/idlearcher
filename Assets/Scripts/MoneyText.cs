﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MoneyText : MonoBehaviour
        #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
        , IPointerClickHandler
    #endif
{
    private Text text;

    private GameState gameState;

    private void Start()
    {
        text = GetComponent<Text>();
        gameState = GameObject.FindWithTag("Game").GetComponent<Game>().State;
    }

    private void OnGUI()
    {
        decimal money = gameState.Money;
        #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
        string t = money.ToString();
        #else
            string t = Helpers.CurrencyString(money);
        #endif
        text.SetText(t);
    }

    #if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
    public void OnPointerClick(PointerEventData eventData)
    {
        gameState.Money *= 2;
    }
    #endif
}