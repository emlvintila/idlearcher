﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArrowPool : MonoBehaviour
{
    public GameObject Object;

    private static int poolId = 0;

    private readonly List<GameObject> objects = new List<GameObject>();

    private GameObject pool;

    private void Start()
    {
        ++poolId;
        pool = new GameObject(nameof(ArrowPool) + poolId);
    }

    public GameObject GetObject()
    {
        GameObject obj = objects.FirstOrDefault(o => o && o.activeInHierarchy == false);
        if (!obj)
        {
            obj = Instantiate(Object, pool.transform);
            objects.Add(obj);
        }

        return obj;
    }

    public void Clear()
    {
        objects.Clear();
    }
}