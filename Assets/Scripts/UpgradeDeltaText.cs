﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpgradeDeltaText : MonoBehaviour
{
    // ReSharper disable once InconsistentNaming
    public UpgradeType UpgradeType;

    private Game game;

    private Upgrade upgrade;

    private Player player;

    private PlayerInfo playerInfo;

    private Text text;

    private void Start()
    {
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        upgrade = GetComponentInParent<Upgrade>();
        player = Resources.FindObjectsOfTypeAll<Player>().First(p => p.Id == upgrade.Id);
        playerInfo = game.State.PlayerInfos.First(p => p.Id == upgrade.Id);
        game.OnUpgrade.AddListener(UpgradeCallback);
        text = GetComponent<Text>();
        UpdateText();
    }

    private void UpgradeCallback(int id, UpgradeType upgradeType)
    {
        if (upgrade.Id == id)
            UpdateText();
    }

    private void UpdateText()
    {
        text.SetText(GetText());
    }

    private string GetText()
    {
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                decimal deltad = (decimal)playerInfo.DamageMultiplier;
                return "+" + Helpers.CurrencyString(deltad);
            case UpgradeType.RateOfFire:
                float deltar = player.ShootInterval * playerInfo.RateOfFireMultiplier;
                return "-" + deltar.ToString("F2");
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
