﻿using UnityEngine;

public class Startup : MonoBehaviour
{
    private void Awake()
    {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;

        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
    }
}
