﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

public static class PlayerInfoFactory
{
    private const int UPGRADES = 21;
    private const float RATE_OF_FIRE_CONST = 1 / 3.04452244f; // 1 / ln(UPGRADES)

    public static PlayerInfo CreateNew(int id, int level, decimal unlockCost, decimal upgradeCostMultiplier, float damageMultiplier = 1, float rateOfFireMultiplier = 1)
    {
        return new PlayerInfo(id, unlockCost)
        {
            DamageMultiplier = damageMultiplier,
            DamageUpgrade = 1,
            RateOfFireMultiplier = rateOfFireMultiplier,
            RateOfFireUpgrade = 1,
            UpgradeCostMultiplier = upgradeCostMultiplier
        };
    }

    [SuppressMessage("ReSharper", "ArgumentsStyleLiteral")]
    [SuppressMessage("ReSharper", "RedundantArgumentDefaultValue")]
    [SuppressMessage("ReSharper", "ArgumentsStyleOther")]
    public static PlayerInfo Create(int id, int level)
    {
        if (level <= 1)
        {
            return CreateNew(id: id, level: level, unlockCost: 0, upgradeCostMultiplier: 5, damageMultiplier: 5,
                rateOfFireMultiplier: 1f / 4 / PlayerInfo.MAX_UPGRADES);
        }

        const long @const = 10;
        const float rofRatio = 3f / 4;
        long x = level;

        //float unlockCost = c * (Mathf.Pow(x, 3) + x);
        //float upgradeCost = c * x * Mathf.Log(x, @base);
        //float damage = c * x * Mathf.Log(Mathf.Sqrt(x), @base);

        decimal unlockCost = (decimal) Mathf.Round(@const * x * x * x * Mathf.Log(x)); // x^3 * ln(x)
        decimal upgradeCost = (decimal) Mathf.Round(@const * x * x); // x^2
        float damage = Mathf.Round(@const * x * Mathf.Log(x)); // x * ln(x)
        float rof = rofRatio * (1 - 1 / (RATE_OF_FIRE_CONST * x + 1)) / PlayerInfo.MAX_UPGRADES;

        return CreateNew(id, level, unlockCost, upgradeCost, damage, rof);
    }

    public static List<PlayerInfo> CreateAll()
    {
        List<PlayerInfo> list = new List<PlayerInfo>(UPGRADES);
        for (int i = 1; i <= UPGRADES; ++i)
        {
            PlayerInfo p = Create(i, i);
            list.Add(p);
        }

        return list;
    }
}