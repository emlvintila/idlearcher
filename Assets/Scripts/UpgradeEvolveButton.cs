﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UpgradeEvolveButton : MonoBehaviour
{
    private Button button;

    private CanvasRenderer canvasRenderer;

    private IEnumerable<CanvasRenderer> canvasRenderers;
    private Upgrade upgrade;

    private void Start()
    {
        upgrade = GetComponentInParent<Upgrade>();
        button = GetComponent<Button>();
        canvasRenderer = GetComponent<CanvasRenderer>();
        canvasRenderers = GetComponentsInChildren<CanvasRenderer>();
    }

    private void Update()
    {
        if (upgrade.CanEvolve())
        {
            if (canvasRenderer.cull)
            {
                Enable();
                button.interactable = !button.interactable;
                button.interactable = !button.interactable;
            }
        }
        else
        {
            if (canvasRenderer.cull == false)
                Disable();
        }
    }

    private void Enable()
    {
        SetState(false);
    }

    private void Disable()
    {
        SetState(true);
    }

    private void SetState(bool state)
    {
        // ReSharper disable once LocalVariableHidesMember
        foreach (CanvasRenderer canvasRenderer in canvasRenderers)
            canvasRenderer.cull = state;
        Canvas.ForceUpdateCanvases();
    }
}
