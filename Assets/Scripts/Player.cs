﻿using System.Collections;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(ArrowPool))]
public class Player : MonoBehaviour
{
    [SerializeField]
    private int id;
    public int Id => id;

    [SerializeField]
    private bool autoShoot;
    public bool AutoShoot
    {
        get { return autoShoot; }
        set { autoShoot = value; }
    }

    [SerializeField]
    private float shootInterval;
    public float ShootInterval
    {
        get { return shootInterval; }
        set { shootInterval = value; }
    }

    [SerializeField]
    private float angleDelta;
    public float AngleDelta
    {
        get { return angleDelta; }
        set { angleDelta = value; }
    }

    [SerializeField]
    private GameObject target;
    public GameObject Target
    {
        get { return target; }
        set { target = value; }
    }

    [SerializeField]
    private Transform emitter;
    public Transform Emitter
    {
        get { return emitter; }
        set { emitter = value; }
    }

    private Game game;

    private PlayerInfo playerInfo;

    private Animator animator;

    private ArrowPool arrowPool;

    private readonly int speedHash = Animator.StringToHash("Speed");

    private void Start()
    {
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        animator = GetComponent<Animator>();
        arrowPool = GetComponent<ArrowPool>();
        playerInfo = game.State.PlayerInfos.First(info => info.Id == Id);
        StartCoroutine(AutoAnimatorSpeedCoroutine());
        //Vector3 euler = emitter.eulerAngles;
        //float delta = Vector3.SignedAngle(target.transform.position - emitter.transform.position, Vector3.right, Vector3.forward);
        //euler.z += delta;
        //emitter.eulerAngles = euler;
    }

    private IEnumerator AutoAnimatorSpeedCoroutine()
    {
        while (true)
        {
            animator.SetFloat(speedHash, 1 / (ShootInterval * (1 - playerInfo.RateOfFire)));
            yield return null;
        }
    }

    private void Shoot()
    {
        Vector3 euler = emitter.rotation.eulerAngles + new Vector3(0, 0, Random.Range(-AngleDelta, AngleDelta));
        GameObject arr = arrowPool.GetObject();
        Rigidbody2D rb = arr.GetComponent<Rigidbody2D>();

        float dist = Vector2.Distance(emitter.position, target.transform.position);
        float angle = Mathf.Deg2Rad * euler.z;
        float x = Mathf.Cos(angle) * dist;
        float h = target.transform.position.y - emitter.transform.position.y;
        float g = Physics.gravity.y * rb.gravityScale;
        float y = Mathf.Sin(angle) * (h - g);
        Vector2 force = rb.mass * new Vector2(x, y);

        arr.GetComponent<Arrow>().SetDamage(playerInfo.Damage);
        arr.transform.SetPositionAndRotation(emitter.position, Quaternion.Euler(euler));
        arr.gameObject.SetActive(true);

        rb.AddForce(force, ForceMode2D.Impulse);
    }
}