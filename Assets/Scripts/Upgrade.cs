﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
    private UpgradeEvolveButton evolveButton;

    private Button evolveButtonButton;

    private Game game;

    private PlayerInfo playerInfo;
        
    [SerializeField]
    private int id;

    // ReSharper disable once ConvertToAutoProperty
    public int Id => id;

    public int Level => playerInfo.Level;

    public int NextLevel => Level + 1;

    private void Start()
    {
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        playerInfo = game.State.PlayerInfos.First(p => p.Id == Id);
        evolveButton = GetComponentInChildren<UpgradeEvolveButton>();
        evolveButtonButton = evolveButton.GetComponent<Button>();
        evolveButtonButton.onClick.AddListener(TryEvolve);
    }

    public bool CanEvolve() =>
        playerInfo.DamageUpgrade == PlayerInfo.MAX_UPGRADES &&
        playerInfo.RateOfFireUpgrade == PlayerInfo.MAX_UPGRADES;

    private void Evolve()
    {
        PlayerInfo temp = PlayerInfoFactory.Create(playerInfo.Id, ++playerInfo.Level);
        playerInfo.DamageUpgrade = temp.DamageUpgrade;
        playerInfo.DamageMultiplier = temp.DamageMultiplier;
        playerInfo.RateOfFireUpgrade = temp.RateOfFireUpgrade;
        playerInfo.RateOfFireMultiplier = temp.RateOfFireMultiplier;
        playerInfo.UpgradeCostMultiplier = temp.UpgradeCostMultiplier;
        game.OnUpgrade.Invoke(Id, UpgradeType.Damage);
        game.OnUpgrade.Invoke(Id, UpgradeType.RateOfFire);
    }

    private void TryEvolve()
    {
        if (CanEvolve())
            Evolve();
    }
}
