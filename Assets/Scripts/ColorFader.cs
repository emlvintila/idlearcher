﻿using System.Collections;
using UnityEngine;

public class ColorFader : MonoBehaviour
{
    public Coroutine Fade(SpriteRenderer spriteRenderer, bool disable, float delay = 0.5f)
    {
        return StartCoroutine(FadeCoroutine(spriteRenderer, disable, delay));
    }

    private static IEnumerator FadeCoroutine(SpriteRenderer spriteRenderer, bool disable, float delay = 0.5f)
    {
        float t = 0;
        Color transparent = spriteRenderer.color;
        transparent.a = 0;
        yield return new WaitForSeconds(delay);
        if (spriteRenderer)
        {
            while (spriteRenderer.color != transparent)
            {
                t += Time.deltaTime;
                spriteRenderer.color = Color.Lerp(spriteRenderer.color, transparent, t);
                yield return null;
            }
        }

        if (disable)
        {
            spriteRenderer.gameObject.SetActive(false);
        }
    }
}