﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpgradeCostText : MonoBehaviour
{
    [SerializeField]
    private UpgradeType upgradeType;
    // ReSharper disable once ConvertToAutoPropertyWhenPossible
    public UpgradeType UpgradeType => upgradeType;

    private int id;

    private Text text;

    private Game game;

    private PlayerInfo playerInfo;

    private void Start()
    {
        id = GetComponentInParent<Upgrade>().Id;
        text = GetComponent<Text>();
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        playerInfo = game.State.PlayerInfos.First(info => info.Id == id);

        game.OnUpgrade.AddListener(OnUpgradeCallback);
        OnUpgradeCallback(id, upgradeType);
    }

    private void OnUpgradeCallback(int idParam, UpgradeType upgradeTypeParam)
    {
        if (idParam == id && upgradeTypeParam == upgradeType)
        {
            if (GetCurrentUpgrade() >= PlayerInfo.MAX_UPGRADES)
            {
                text.SetText("MAX");
            }
            else
            {
                text.SetText(Helpers.CurrencyString(playerInfo.GetCostForNext(GetCurrentUpgrade())));
            }
        }
    }

    private int GetCurrentUpgrade()
    {
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                return playerInfo.DamageUpgrade;
            case UpgradeType.RateOfFire:
                return playerInfo.RateOfFireUpgrade;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
