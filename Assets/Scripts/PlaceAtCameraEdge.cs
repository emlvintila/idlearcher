﻿using UnityEngine;

public class PlaceAtCameraEdge : MonoBehaviour
{
    public RectTransform.Edge Edge;

    private void Start()
    {
        PlaceAtEdge(gameObject, Camera.main, Edge);
    }

    private static void PlaceAtEdge(GameObject gameObject, Camera camera, RectTransform.Edge edge)
    {
        float x = gameObject.transform.position.x;
        float y = gameObject.transform.position.y;

        ViewportHandler vh = camera.GetComponent<ViewportHandler>();

        if (edge.HasFlag(RectTransform.Edge.Left))
        {
            if (vh)
            {
                x = vh.BottomLeft.x;
            }
            else
            {
                x = camera.ScreenToWorldPoint(Vector3.zero).x;
            }
        }

        if (edge.HasFlag(RectTransform.Edge.Right))
        {
            if (vh)
            {
                x = vh.BottomRight.x;
            }
            else
            {
                x = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, 0)).x;
            }
        }

        if (edge.HasFlag(RectTransform.Edge.Top))
        {
            if (vh)
            {
                y = vh.TopLeft.y;
            }
            else
            {
                y = camera.ScreenToWorldPoint(Vector3.zero).y;
            }
        }

        if (edge.HasFlag(RectTransform.Edge.Bottom))
        {
            if (vh)
            {
                y = vh.BottomLeft.y;
            }
            else
            {
                y = camera.ScreenToWorldPoint(new Vector3(0, camera.pixelHeight)).y;
            }
        }

        gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
    }
}