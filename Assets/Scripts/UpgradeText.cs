﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpgradeText : MonoBehaviour
{
    [SerializeField]
    private UpgradeType upgradeType;
    // ReSharper disable once ConvertToAutoPropertyWhenPossible
    public UpgradeType UpgradeType => upgradeType;

    private int id;

    private Text text;

    private Game game;

    private PlayerInfo playerInfo;

    private Player player;
    
    private void Start()
    {
        id = GetComponentInParent<Upgrade>().Id;
        text = GetComponent<Text>();
        game = GameObject.FindWithTag("Game").GetComponent<Game>();
        playerInfo = game.State.PlayerInfos.First(info => info.Id == id);
        player = Resources.FindObjectsOfTypeAll<Player>().First(p => p.Id == id);

        game.OnUpgrade.AddListener(OnUpgradeCallback);
        OnUpgradeCallback(id, upgradeType);
    }

    private void OnUpgradeCallback(int idParam, UpgradeType upgradeTypeParam)
    {
        if (idParam == id && upgradeTypeParam == upgradeType)
        {
            text.SetText(GetText());
        }
    }

    private float GetValue()
    {
        
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                return playerInfo.Damage;
            case UpgradeType.RateOfFire:
                return playerInfo.RateOfFire;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private string GetText()
    {
        float val = GetValue();
        switch (UpgradeType)
        {
            case UpgradeType.Damage:
                return Helpers.CurrencyString((decimal) val);
            case UpgradeType.RateOfFire:
                return (player.ShootInterval * (1 - val)).ToString("F2");
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
